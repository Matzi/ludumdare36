﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {
    float timer = 10.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;

        if (timer <= 0.0f || Input.GetAxis("Fire1") > 0.0f)
        {
            SceneManager.LoadScene("Game");
            timer = 1000.0f;
        }
	}
}
