﻿using UnityEngine;
using System.Collections;
using System;

public class Combatant : MonoBehaviour
{
    public GameObject projectile;
    public GameObject attackPFX;

    public float MaxHealth = 100.0f;
    public float Damage = 60.0f;
    public float Armor = 0.0f;
    public float Regeneration = 0.0f;
    public float DamageMultiplier = 1.0f;
    public bool IsDead = false;
    public float attackDistance = 2.3f;
    public float AttackHeight = 3.0f;
    public float attackTime = 3.5f;
    public float attackAngle = 60.0f;
    public float hitTime = 1.0f;

    public bool wasDamaged = false;

    CreatureManager manager;

    float attackTimer = 0.0f;
    public float Health = 100.0f;
    bool duringAttack = false;
    internal bool shoot = false;

    // Use this for initialization
    void Start()
    {
        Health = MaxHealth;
        manager = FindObjectOfType<CreatureManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (IsDead)
        {
            return;
        }

        if (Health <= 0.0f)
        {
            Health = 0.0f;
            IsDead = true;
            return;
        }

        Heal(Regeneration * Time.deltaTime);

        if (attackTimer > 0.0f)
        {
            attackTimer -= Time.deltaTime;
            if (duringAttack && attackTimer <= attackTime - hitTime)
            {
                duringAttack = false;
                manager.Attack(this);
            }
        }
    }

    public void Heal(float reg)
    {
        if (IsDead)
        {
            return;
        }

        Health += reg;

        if (Health > MaxHealth)
        {
            Health = MaxHealth;
        }

        if (Health <= 0.0f)
        {
            Health = 0.0f;
            IsDead = true;
        }
    }

    public void LaunchAttack(bool shootAttack = true)
    {
        shoot = shootAttack && (projectile != null);
        if (CanMove())
        {
            attackTimer = attackTime;
            duringAttack = true;
        }
    }

    public bool CanMove()
    {
        return attackTimer <= 0.0f && !IsDead;
    }

    public bool DoDamage(Combatant other)
    {
        if (IsDead || other.IsDead)
        {
            return false;
        }

        other.Health -= Mathf.Max(0.0f, (Damage * DamageMultiplier) - other.Armor);
        other.wasDamaged = true;

        if (other.Health <= 0.0f)
        {
            other.Health = 0.0f;
            other.IsDead = true;
            other.Die();
        }

        return other.IsDead;
    }

    public bool DoDamage(float damage)
    {
        if (IsDead)
        {
            return false;
        }

        Health -= Mathf.Max(0.0f, (damage * DamageMultiplier));
        wasDamaged = true;

        if (Health <= 0.0f)
        {
            Health = 0.0f;
            IsDead = true;
            Die();
        }

        return IsDead;
    }

    public void Improve(float health, float damage)
    {
        if (IsDead)
        {
            return;
        }

        Health += health;
        MaxHealth += health;
        Damage += damage;
    }

    public void Reset()
    {
        Health = MaxHealth;
        IsDead = false;
    }

    internal void Die()
    {
        Character ch = GetComponentInChildren<Character>();
        if (ch != null)
        {
            ch.OnDeath();
        }
        else
        {
            foreach (ParticleSystem pfx in GetComponentsInChildren<ParticleSystem>())
            {
                pfx.Play();
            }
            foreach (AudioSource sfx in GetComponentsInChildren<AudioSource>())
            {
                sfx.Play();
                sfx.time = 1.0f;
            }
        }
    }
}
