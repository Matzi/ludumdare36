﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour
{


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -20)
        {
            OnTriggerEnter(null);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        foreach (MeshRenderer mesh in GetComponentsInChildren<MeshRenderer>())
        {
            mesh.enabled = false;
        }
        foreach (SphereCollider collider in GetComponentsInChildren<SphereCollider>())
        {
            collider.enabled = false;
        }
        foreach (Rigidbody body in GetComponentsInChildren<Rigidbody>())
        {
            Destroy(body, 0.01f);
        }

        foreach (ParticleSystem pfx in GetComponentsInChildren<ParticleSystem>())
        {
            pfx.Play();
        }

        foreach (AudioSource sfx in GetComponentsInChildren<AudioSource>())
        {
            sfx.Play();
        }

        Destroy(gameObject, 3.0f);

        FindObjectOfType<Character>().Detonate();
    }
}
