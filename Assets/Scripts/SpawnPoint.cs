﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour {
	public bool IsActive = true;
    public float Period = 5.0f;

	float time = 0.0f;

	// Use this for initialization
	void Start () {
		FindObjectOfType<CreatureManager>().AddSpawnPoint(gameObject);
	}

	// Update is called once per frame
	void Update () {
		if (time > 0.0f)
		{
			time -= Time.deltaTime;
			if (time <= 0.0f)
			{
				IsActive = true;
			}
		}
	}

	public void Deactivate()
	{
		IsActive = false;
		time = Period;
	}

	void OnTriggerEnter(Collider other)
	{
        FindObjectOfType<CreatureManager>().RemoveSpawnPoint(gameObject);
        Destroy(gameObject);
	}

	void OnTriggerLeave(Collider other)
	{
	}
}
