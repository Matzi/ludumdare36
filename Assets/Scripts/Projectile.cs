﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public float ProjectileSpeed = 100.0f;
    public bool Fling = false;
    Combatant combatant;

    // Use this for initialization
    void Start()
    {
        combatant = GetComponentInChildren<Combatant>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        DestroyObject(gameObject, 0.6f);

        Combatant enemy = other.GetComponentInChildren<Combatant>();
        if (enemy != null && enemy != combatant)
        {
            if (Fling)
            {
                Creature cr = enemy.GetComponentInChildren<Creature>();
                if (cr != null)
                {
                    Vector3 dir = cr.transform.position - combatant.transform.position;
                    float up = dir.magnitude;
                    cr.Fling(Vector3.up * (60.0f - up) * 2.0f + dir);
                }
                else
                {
                    combatant.DoDamage(enemy);
                }

                FindObjectOfType<CameraShake>().Shake(0.5f);
            }
            else
            {
                combatant.DoDamage(enemy);
            }
        }
        Destroy(this);
        Destroy(GetComponentInChildren<Rigidbody>());
        Destroy(GetComponentInChildren<MeshRenderer>());
        foreach (ParticleSystem pfx in GetComponentsInChildren<ParticleSystem>())
        {
            pfx.Play();
        }

        foreach (AudioSource sfx in GetComponentsInChildren<AudioSource>())
        {
            sfx.Play();
        }
    }

}