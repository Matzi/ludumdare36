﻿using UnityEngine;
using System.Collections;
using System;

public class Character : MonoBehaviour
{
    public GameObject endBomber;
    public GameObject dust;
    public bool NormalStart = true;

    Vector3 movementTargetPosition;
    Animator golemAnimator;
    CharacterController ctrl;
    CreatureManager manager;
    Combatant combatant;
    GUIStyle labelStyle;
    GUIStyle textStyle;
    AudioSource stomp;
    Storm storm;
    GameObject plane;


    Quaternion targetRotation;
    float regen = 0;
    float rotateSpeed = 10.0f;
    float walkSpeed = 19.0f;
    float stepInterval = 0.53f;
    float stepTimer = 0;
    float flashTimer = 0.0f;
    float endTimer = 8.0f;
    float easeIn = 0.0f;
    int deathCounter = 0;
    int cycleCounter = 0;
    bool showText = false;
    bool showDeath = false;
    Texture2D flash;

    string[] DeathTexts = new string[]{
        "Death does not embrace me\nI was built to endure",
        "The rest is momentary\nMy task is unfinished",
        "Failure is not an option",
        "I am feeling weak\nMust do better",
        "Got a bit rusty over the cycles",
        "I must be controlled by a crippled mind",
        "Git Gud!"
    };

    string[] CycleTexts = new string[]
    {
        "After all these Cycles yet another awakening\nI was designed to keep balance by force",
        "Countless deaths mean nothing to the higher goal\nI am a protector yet I have failed many times",
        "The progress of technology is inevitable\nYet my purpose is to stop it from devouring the world",
        "It will never end and I cannot rest\nBut I cannot succeed either",
        "The end is near but it won't be mine\nI am doomed to watch and strugle",
        "My existence is futile\nI raise again in the coming Cycle",
        "I keep fighting but to what end?",
        "This repetition is unbearable",
        "How long do I have to suffer?",
        "..."
    };


    // Use this for initialization
    void Start()
    {
        manager = GetComponentInChildren<CreatureManager>();
        ctrl = GetComponentInChildren<CharacterController>();
        combatant = GetComponentInChildren<Combatant>();
        movementTargetPosition = Vector3.zero;
        golemAnimator = GetComponentInChildren<Animator>();
        stomp = transform.FindChild("Steps").GetComponentInChildren<AudioSource>();
        targetRotation = transform.Find("Golem").rotation;
        storm = GetComponentInChildren<Storm>();
        regen = combatant.Regeneration;
        if (NormalStart)
        {
            storm.Intro();
            SwitchMap(0);
            showText = true;
            golemAnimator.SetBool("Burrow", true);
            StartDust();
            easeIn = 3.0f;
        }
        else
        {
            manager.CurrentLevel = 4;
        }

        endTimer = 8.0f + (manager.CurrentLevel == 4 ? 10 : 0);

        labelStyle = new GUIStyle();
        labelStyle.fontStyle = FontStyle.Bold;
        labelStyle.fontSize = 24;
        labelStyle.normal.textColor = Color.white;

        textStyle = new GUIStyle();
        textStyle.fontStyle = FontStyle.Bold;
        textStyle.fontSize = 24;
        textStyle.normal.textColor = Color.white;

        flash = new Texture2D(1, 1); //Creates 2D texture
        flash.SetPixel(1, 1, Color.white); //Sets the 1 pixel to be white
        flash.Apply(); //Applies all the changes made

        stepTimer = stepInterval;
    }

    // Update is called once per frame
    void Update()
    {
        if (flashTimer > 0.0f)
        {
            flashTimer -= Time.deltaTime;
        }

        if (combatant.IsDead || storm.IsRunning())
        {
            golemAnimator.SetBool("Burrow", true);
            golemAnimator.SetBool("Moving", false);
            easeIn = 3.0f;
            return;
        }

        if (manager.IsDone())
        {
            if (endTimer > 0.0f)
            {
                if (manager.CurrentLevel == 4 && plane == null)
                {
                    plane = (GameObject)GameObject.Instantiate(endBomber, transform.position + Vector3.up * 55 - Vector3.forward * 1000.0f, Quaternion.identity);
                }

                endTimer -= Time.deltaTime;
            }
            else
            {
                int map = (manager.CurrentLevel + 1) % 5;
                SwitchMap(map);
            }
        }

        float horizontal = Mathf.Clamp(Input.GetAxis("Horizontal"), -1, 1);
        float vertical = Mathf.Clamp(Input.GetAxis("Vertical"), -1, 1);

        if (easeIn <= 0.0f)
        {
            movementTargetPosition += Vector3.forward * horizontal;
            movementTargetPosition += Vector3.left * vertical;
        }
        else
        {
            easeIn -= Time.deltaTime;
        }
        if (movementTargetPosition.sqrMagnitude > 0.5f) { movementTargetPosition.Normalize(); }

        if (combatant.CanMove())
        {
            if (Input.GetAxis("Fire1") > 0.0f)
            {
                golemAnimator.SetTrigger("Attack");
                golemAnimator.SetBool("Moving", false);
                stepTimer = stepInterval;

                combatant.LaunchAttack();
            }
            /*if (Input.GetAxis("Fire2") > 0.0f)
            {
                golemAnimator.SetTrigger("Shoot");
                golemAnimator.SetBool("Moving", false);
                stepTimer = stepInterval;

                combatant.LaunchAttack(false);
            }*/
            
            /*if (Input.GetKeyUp("1")) { SwitchMap(0); showText = true; }
            if (Input.GetKeyUp("2")) { SwitchMap(1); showText = true; }
            if (Input.GetKeyUp("3")) { SwitchMap(2); showText = true; }
            if (Input.GetKeyUp("4")) { SwitchMap(3); showText = true; }
            if (Input.GetKeyUp("5")) { SwitchMap(4); showText = true; }*/
        }

        Transform trans = transform.Find("Golem");
        Quaternion tempRot = trans.rotation;

        if (movementTargetPosition.sqrMagnitude > 0.5f)
        {
            Vector3 turn = movementTargetPosition;

            if (turn.sqrMagnitude > 0.1f)
            {
                trans.LookAt(trans.position + turn, Vector3.up);
                targetRotation = trans.rotation;
            }

            golemAnimator.SetBool("Moving", true);

            if (stepTimer <= 0.0f)
            {
                stepTimer = stepInterval;
                stomp.Play();
            }
            stepTimer -= Time.deltaTime;

            if (combatant.CanMove())
            {
                ctrl.Move(movementTargetPosition * (Time.deltaTime * walkSpeed));
            }
        }
        else
        {
            golemAnimator.SetBool("Moving", false);
            stepTimer = stepInterval;
        }

        trans.rotation = Quaternion.Slerp(tempRot, targetRotation, Time.deltaTime * rotateSpeed);
        ctrl.Move(Vector3.down * (Time.deltaTime * 100.0f));

        movementTargetPosition = Vector3.zero;
    }

    void SwitchMap(int map)
    {
        manager.StopSpawn(map);
        storm.Trigger(map);
        combatant.Regeneration = 20.0f;
        combatant.IsDead = false;
        combatant.Heal(1);
    }

    public void AddScore(int score)
    {

    }

    public void OnDeath()
    {
        SwitchMap(manager.CurrentLevel);
        StartDust();
        showDeath = true;
    }

    public void OnTransientDone()
    {
        combatant.Regeneration = regen;

        if (showDeath)
        {
            ++deathCounter;
            showDeath = false;
        }
        else
        {
            ++cycleCounter;
            showText = true;
        }
        golemAnimator.SetBool("Burrow", false);
        StartDust();
        endTimer = 8.0f + (manager.CurrentLevel == 4 ? 10 : 0);
        Destroy(plane);
        plane = null;
        easeIn = 3.0f;
    }

    void StartDust()
    {
        if (dust != null)
        {
            Transform trans = transform.Find("Golem");
            GameObject obj = (GameObject)GameObject.Instantiate(dust, trans.position + Vector3.up * 2, Quaternion.identity);
            Destroy(obj, 5.0f);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        Combatant enemy = other.GetComponentInChildren<Combatant>();
        Projectile pr = other.GetComponentInChildren<Projectile>();
        if (enemy != null && enemy != combatant && pr == null)
        {
            combatant.DoDamage(enemy);
        }
    }

    void OnGUI()
    {
        string hpString = "Health: " + (int)combatant.Health + " / " + (int)combatant.MaxHealth;
        GUI.Label(new Rect(10, 5, 1000, 40), hpString, labelStyle);

        if (flashTimer > 0.0f)
        {
            float flashAlpha = flashTimer;
            flash.SetPixel(1, 1, new Color(1.0f, 1.0f, 1.0f, flashAlpha));
            flash.Apply();

            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), flash);
        }

        if (showDeath || showText)
        {
            string text;

            if (showDeath)
            {
                text = DeathTexts[deathCounter % DeathTexts.Length];
            }
            else
            {
                text = CycleTexts[cycleCounter % CycleTexts.Length];
            }

            textStyle = new GUIStyle();
            textStyle.fontStyle = FontStyle.Bold;
            textStyle.fontSize = 24;
            textStyle.normal.textColor = new Color(1.0f, 1.0f, 1.0f, storm.GetTransient());
            textStyle.alignment = TextAnchor.MiddleCenter;

            GUI.Label(new Rect(0, 0, Screen.width, Screen.height), text, textStyle);
        }
    }

    internal void Detonate()
    {
        flashTimer = 5.0f;
        endTimer = 3.0f;
    }
}
