﻿using UnityEngine;
using System.Collections;

public class Creature : MonoBehaviour {
	public int SpawnWeight = 10;
	public float runSpeed = 14.0f;

	Animator animator;
	CharacterController ctrl;
	CreatureManager manager;
	Combatant combatant;
    GameObject selfDestruct;
	Vector3 targetPosition;
	float rotateSpeed = 90.0f;
    Vector3 impulse = Vector3.zero;
	bool killed = false;

	// Use this for initialization
	void Start () {
		manager = FindObjectOfType<CreatureManager>();
		ctrl = GetComponentInChildren<CharacterController>();
		combatant = GetComponentInChildren<Combatant>();
		manager.AddCreatrure(gameObject);
		animator = GetComponentInChildren<Animator>();
        selfDestruct = transform.Find("SelfDestruct").gameObject;
        selfDestruct.SetActive(false);

    }

	// Update is called once per frame
	void Update () {
		if (combatant.IsDead)
		{
			if (killed)
			{
				return;
			}
			GetComponentInChildren<CharacterController>().enabled = false;
			manager.RemoveCreatrure(gameObject);

			killed = true;
			Destroy(gameObject, 20.0f);

			Transform obj = transform.Find("Body");

			if (obj != null)
			{
				obj.gameObject.SetActive(false);
			}

			return;
		}

		Quaternion tempRot = transform.rotation;
		transform.LookAt(targetPosition);
		Quaternion hitRot = transform.rotation;

		transform.rotation = Quaternion.Slerp(tempRot, hitRot, Time.deltaTime * rotateSpeed);

        impulse += Physics.gravity * Time.deltaTime;
        ctrl.Move(impulse * Time.deltaTime);

        if (!ctrl.isGrounded)
        {
            if (impulse.sqrMagnitude > 1000.0f)
            {
                selfDestruct.SetActive(true);
            }
            return;
        }
        else
        {
            impulse = Vector3.zero;
        }

        if (combatant.CanMove())
		{
			Vector3 dist = (gameObject.transform.position - targetPosition);

			if (dist.sqrMagnitude > 0.1f)
			{
				if (dist.sqrMagnitude < combatant.attackDistance * combatant.attackDistance)
				{
					combatant.LaunchAttack();
					animator.SetTrigger("Attack");
				}
				else
				{
					ctrl.Move((targetPosition - transform.position).normalized * Time.deltaTime * runSpeed);
				}
			}
		}
	}

    internal void Fling(Vector3 direction)
    {
        impulse += direction;
    }

	public void Notice(GameObject obj, int countClose)
	{
        Combatant cb = obj.GetComponent<Combatant>();
        if (cb && !cb.IsDead)
		{
			targetPosition = obj.transform.position;
			targetPosition.y = gameObject.transform.position.y;
		}
        else
        {
            targetPosition = gameObject.transform.position;
        }
	}
}
