﻿using UnityEngine;
using System.Collections;

public class Suicide : MonoBehaviour
{
    public float Damage = 1000.0f;
    float Timer = 6.0f;
    float angularSpeed;
    Vector3 axis;
    CharacterController ctrl;

    // Use this for initialization
    void Start()
    {
        ctrl = GetComponentInParent<CharacterController>();
        axis = new Vector3(Random.Range(0.3f, +1.0f), 0.001f, Random.Range(0.3f, +1.0f));
        angularSpeed = Random.Range(100.0f, 200.0f) * (Random.Range(0, 2) == 0 ? -1.0f : +1.0f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.parent.Find("Body").Rotate(axis, angularSpeed * Time.deltaTime);
        Timer -= Time.deltaTime;
        if (ctrl.isGrounded || Timer < 0.0f)
        {
            GetComponentInParent<Combatant>().DoDamage(Damage);
            Destroy(this);
        }
    }
}

