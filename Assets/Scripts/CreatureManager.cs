﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CreatureManager : MonoBehaviour
{
    public List<GameObject> Enemies_1;
    public List<GameObject> Enemies_2;
    public List<GameObject> Enemies_3;
    public List<GameObject> Enemies_4;
    public List<GameObject> Enemies_5;
    public float MaxEnemyCount = 20;
    public float HealthBonusPerSecond = 0.15f;
    public float DamageBonusPerSecond = 0.05f;
    public float weightBonusPerSecond = 0.06f;
    public int CurrentLevel = 0;

    List<List<GameObject>> Enemies;
    List<GameObject> creatures;
    List<GameObject> spawnPoints;

    Combatant combatant;
    float distanceThreshold = 10.0f;
    float totalTime = 0.0f;
    float cooldown = 0.0f;

    CreatureManager()
    {
        creatures = new List<GameObject>();
        spawnPoints = new List<GameObject>();
    }

    // Use this for initialization
    void Start()
    {
        Enemies = new List<List<GameObject>>();
        Enemies.Add(Enemies_1);
        Enemies.Add(Enemies_2);
        Enemies.Add(Enemies_3);
        Enemies.Add(Enemies_4);
        Enemies.Add(Enemies_5);
        combatant = GetComponentInChildren<Combatant>();
    }

    // Update is called once per frame
    void Update()
    {
        totalTime += Time.deltaTime;
        cooldown -= Time.deltaTime;
        if (cooldown > 0.0f)
        {
            return;
        }
        else
        {
            cooldown = 0.0f;
        }

        int close = 0;
        foreach (GameObject creature in creatures)
        {
            Vector3 dist = (creature.transform.position - gameObject.transform.position);
            dist.y = 0;
            if (!combatant.IsDead && !creature.GetComponentInChildren<Combatant>().IsDead &&
                dist.sqrMagnitude < distanceThreshold * distanceThreshold)
            {
                ++close;
            }
        }
        foreach (GameObject creature in creatures)
        {
            Creature c = creature.GetComponent<Creature>();
            if (!c.GetComponentInChildren<Combatant>().IsDead)
            {
                c.Notice(gameObject, close);
            }
        }

        if (creatures.Count < MaxEnemyCount && Enemies[CurrentLevel].Count > 0 && spawnPoints.Count > 0)
        {
            int spawnBias = (int)(totalTime * weightBonusPerSecond);
            int number = Random.Range(0, 100 + spawnBias * Enemies[CurrentLevel].Count);
            GameObject spawn = spawnPoints[Random.Range(0, spawnPoints.Count)];
            SpawnPoint sp = spawn.GetComponentInChildren<SpawnPoint>();
            if (sp != null && sp.IsActive)
            {
                int index = -1;

                do
                {
                    index = (index + 1) % Enemies[CurrentLevel].Count;

                    number -= Enemies[CurrentLevel][index].GetComponentInChildren<Creature>().SpawnWeight + spawnBias;
                } while (number > 0);

                GameObject obj = (GameObject)Object.Instantiate(Enemies[CurrentLevel][index], spawn.transform.position, Quaternion.identity);

                obj.GetComponentInChildren<Combatant>().Improve(totalTime * HealthBonusPerSecond, totalTime * DamageBonusPerSecond);
                sp.Deactivate();
            }
        }
    }

    internal bool IsDone()
    {
        return spawnPoints.Count == 0 && creatures.Count == 0;
    }

    public void AddCreatrure(GameObject creature)
    {
        creatures.Add(creature);
    }

    public void RemoveCreatrure(GameObject creature)
    {
        creatures.Remove(creature);
    }

    public void AddSpawnPoint(GameObject gameObject)
    {
        spawnPoints.Add(gameObject);
    }

    public void RemoveSpawnPoint(GameObject gameObject)
    {
        spawnPoints.Remove(gameObject);
    }

    public void StopSpawn(int nextLevel)
    {
        cooldown = 14.0f;
        CurrentLevel = nextLevel;
        spawnPoints.Clear();
        foreach (GameObject cr in creatures)
        {
            Destroy(cr.GetComponentInChildren<Creature>());
            Destroy(cr, 5.0f);
        }

        creatures.Clear();
    }

    public void Attack(Combatant cmb)
    {
        CharacterController ctrl = cmb.GetComponentInChildren<CharacterController>();

        if (ctrl != null && !ctrl.isGrounded)
        {
            return;
        }

        if (cmb == combatant)
        {
            PlayerAttack(cmb);
        }
        else
        {
            EnemyAttack(cmb);
        }
    }

    void PlayerAttack(Combatant ch)
    {
        if (!ch.shoot)
        {
            Transform trans = transform.Find("Golem");
            foreach (GameObject creature in creatures)
            {
                Combatant cmb = creature.GetComponentInChildren<Combatant>();
                Vector3 dist = (creature.transform.position - ch.transform.position);
                dist.y = 0.0f;

                float angle = Vector3.Angle(trans.forward, dist.normalized);

                if (cmb && (dist.sqrMagnitude < (ch.attackDistance * ch.attackDistance)) && (angle <= ch.attackAngle * 0.5f))
                {
                    if (ch.DoDamage(cmb))
                    {
                        GetComponent<Character>().AddScore(1);
                    }
                }
            }

            if (ch.attackPFX != null)
            {
                GameObject obj = (GameObject)GameObject.Instantiate(ch.attackPFX, ch.transform.position, trans.rotation);
                Destroy(obj, 2.0f);
            }
        }
        else
        {
            GameObject obj = (GameObject)GameObject.Instantiate(ch.projectile, ch.transform.position + Vector3.up, ch.transform.rotation);
        }
    }

    void EnemyAttack(Combatant cr)
    {
        Vector3 dist = (gameObject.transform.position - cr.transform.position + Vector3.up * 5);

        float angle = Vector3.Angle(cr.transform.forward, dist.normalized);

        if (combatant == null)
        {
            return;
        }

        if (cr.projectile == null)
        {
            if ((dist.sqrMagnitude < (cr.attackDistance * cr.attackDistance)) && (angle <= cr.attackAngle * 0.5f))
            {
                cr.DoDamage(combatant);
            }
        }
        else
        {
            GameObject obj = (GameObject)GameObject.Instantiate(cr.projectile, cr.transform.position + Vector3.up * cr.AttackHeight, cr.transform.rotation);
            float speed = cr.projectile.GetComponentInChildren<Projectile>().ProjectileSpeed;
            float time = dist.magnitude / speed;
            float gravity = -0.5f * Physics.gravity.y * time * time;
            Vector3 direction = (dist + Vector3.up * (gravity * 2 - cr.AttackHeight)).normalized * speed;
            obj.GetComponentInChildren<Rigidbody>().velocity = direction;
        }
    }
}
