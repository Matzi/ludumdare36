﻿using UnityEngine;
using System.Collections;

public class Collapse : MonoBehaviour {
	GameObject ruin;

	// Use this for initialization
	void Start () {
		ruin = transform.FindChild("Ruin").gameObject;
		ruin.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		foreach (BoxCollider collider in GetComponentsInChildren<BoxCollider>())
		{
			collider.enabled = false;
		}

		foreach (Transform whole in transform.FindChild("Whole"))
		{
			whole.gameObject.SetActive(false);
		}

		ruin.gameObject.SetActive(true);

		foreach (ParticleSystem pfx in GetComponentsInChildren<ParticleSystem>())
		{
			pfx.Play();
		}

		foreach (AudioSource sfx in GetComponentsInChildren<AudioSource>())
		{
			sfx.Play();
			sfx.time = 0.5f;
		}
	}
}
