﻿using UnityEngine;
using System.Collections;

public class Bomber : MonoBehaviour {
    public GameObject Bomb;

    bool bombDropped = false;
    float distance = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float movement = Time.deltaTime * 300.0f;
        distance += movement;
        transform.position += Vector3.forward * movement;

        if (!bombDropped && distance > 999)
        {
            bombDropped = true;
            GameObject.Instantiate(Bomb, transform.position, Quaternion.identity);
        }
	}
}
