﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{

    public Vector3 camTransform;
    public float shakeDuration = 0f;
    public float shakeAmount = 2.2f;
    
    Vector3 originalPos;

    void Awake()
    {
        if (camTransform == null)
        {
            camTransform = transform.localPosition;
        }
    }

    void OnEnable()
    {
        originalPos = transform.localPosition;
    }

    public void Shake(float duration)
    {
        shakeDuration = duration;
    }

    void Update()
    {
        if (shakeDuration > 0.0f)
        {
            transform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;
            shakeDuration -= Time.deltaTime;
        }
        else
        {
            shakeDuration = 0.0f;
            transform.localPosition = originalPos;
        }
    }
}