﻿using UnityEngine;
using System.Collections;

public class Storm : MonoBehaviour
{
    public GameObject[] Maps;
    public float Length = 15.0f;
    public float FadeIn = 3.0f;
    public float FadeOut = 3.0f;

    Vector3 StartPos;
    Vector3 Target;
    GameObject oldMap;
    GameObject nextMap;
    Camera cam;
    float transient = 0.0f;

    float timer = 0.0f;
    bool runs = false;

    // Use this for initialization
    void Start()
    {
        cam = transform.parent.GetComponentInChildren<Camera>();
    }

    void Awake()
    {
        foreach (AudioSource sfx in GetComponentsInChildren<AudioSource>())
        {
            sfx.volume = 0;
        }
    }

    public void Intro()
    {
        nextMap = (GameObject)GameObject.Instantiate(Maps[5], Vector3.zero, Quaternion.identity);
        transform.parent.position = nextMap.transform.Find("StartPosition").position;
    }

    public void Trigger(int mapIndex)
    {
        if (oldMap != null)
        {
            Destroy(oldMap);
        }
        oldMap = nextMap;
        foreach (AudioSource sfx in GetComponentsInChildren<AudioSource>())
        {
            sfx.Play();
            sfx.volume = 0;
        }
        foreach (ParticleSystem pfx in GetComponentsInChildren<ParticleSystem>())
        {
            pfx.Play();
        }
        nextMap = (GameObject)GameObject.Instantiate(Maps[mapIndex], Vector3.zero, Quaternion.identity);
        Target = nextMap.transform.Find("StartPosition").position;
        nextMap.SetActive(false);

        StartPos = transform.parent.position;
        runs = true;
        timer = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!runs)
        {
            return;
        }
        if (timer > Length)
        {
            runs = false;
            GetComponentInParent<Character>().OnTransientDone();
            return;
        }

        timer += Time.deltaTime;
        float volume = 1.0f;
        float up = StartPos.y;
        float tween = 0.0f;
        Vector3 pos = StartPos;

        if (timer < FadeIn)
        {
            volume = timer / FadeIn;
        }
        else if (timer > Length - FadeOut)
        {
            volume = (Length - timer) / FadeOut;
        }
        foreach (AudioSource sfx in GetComponentsInChildren<AudioSource>())
        {
            sfx.volume = volume;
        }

        if (timer > FadeIn && timer < FadeIn * 2)
        {
            up = StartPos.y + (timer / FadeIn - 1) * 100.0f;
        }
        if (timer > Length - FadeOut * 2 && timer < Length - FadeOut)
        {
            up = Target.y + ((Length - timer) / FadeOut - 1) * 100.0f;
        }
        tween = Mathf.Clamp((timer - FadeIn * 2) / (Length - (FadeIn + FadeOut) * 2), 0.0f, 1.0f);
        if (timer > FadeIn * 2 && timer < Length - FadeOut * 2)
        {
            if (oldMap != null)
            {
                nextMap.SetActive(true);
                Destroy(oldMap, 1.0f);
                oldMap = null;
            }
            up = Mathf.Lerp(StartPos.y, Target.y, tween) + 100.0f;
        }

        transient = Mathf.Clamp(Mathf.Min(timer, Length - timer) / FadeIn, 0.0f, 1.0f);
        cam.fieldOfView = Mathf.Lerp(60, 50, transient);

        pos = (StartPos * (1.0f - tween) + Target * tween);
        pos.y = up;

        transform.parent.position = pos;
    }

    public bool IsRunning()
    {
        return runs;
    }

    public float GetTransient()
    {
        return transient;
    }
}
