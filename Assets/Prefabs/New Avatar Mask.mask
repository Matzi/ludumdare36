%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: New Avatar Mask
  m_Mask: 01000000000000000100000001000000010000000000000000000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Circle001
    m_Weight: 1
  - m_Path: Circle001/Circle003
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/Cylinder008
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/Cylinder013
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/Cylinder016
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/GeoSphere010
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/GeoSphere010/GeoSphere009
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/GeoSphere010/GeoSphere009/Box042
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/GeoSphere010/GeoSphere009/Box042/Box049
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/GeoSphere010/GeoSphere009/Box042/Box049/Box047
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/GeoSphere010/GeoSphere009/Box044
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/GeoSphere010/GeoSphere009/Box044/Box043
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/GeoSphere010/GeoSphere009/Box044/Box043/Box045
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/GeoSphere010/GeoSphere009/Box048
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/GeoSphere010/GeoSphere009/Box048/Box050
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/GeoSphere010/GeoSphere009/Box048/Box050/Box046
    m_Weight: 1
  - m_Path: Circle001/Circle003/Box041/GeoSphere015
    m_Weight: 0
  - m_Path: Circle001/Circle003/Box041/GeoSphere015/GeoSphere016
    m_Weight: 0
  - m_Path: Circle001/Circle003/Box041/GeoSphere015/GeoSphere016/Box051
    m_Weight: 0
  - m_Path: Circle001/Circle003/Box041/GeoSphere015/GeoSphere016/Box051/Box052
    m_Weight: 0
  - m_Path: Circle001/Circle003/Box041/GeoSphere015/GeoSphere016/Box051/Box052/Box053
    m_Weight: 0
  - m_Path: Circle001/Circle003/Box041/GeoSphere015/GeoSphere016/Box054
    m_Weight: 0
  - m_Path: Circle001/Circle003/Box041/GeoSphere015/GeoSphere016/Box054/Box055
    m_Weight: 0
  - m_Path: Circle001/Circle003/Box041/GeoSphere015/GeoSphere016/Box054/Box055/Box056
    m_Weight: 0
  - m_Path: Circle001/Circle003/Box041/GeoSphere015/GeoSphere016/Box057
    m_Weight: 0
  - m_Path: Circle001/Circle003/Box041/GeoSphere015/GeoSphere016/Box057/Box058
    m_Weight: 0
  - m_Path: Circle001/Circle003/Box041/GeoSphere015/GeoSphere016/Box057/Box058/Box059
    m_Weight: 0
  - m_Path: Circle001/Cylinder009
    m_Weight: 1
  - m_Path: Circle001/Cylinder009/GeoSphere008
    m_Weight: 1
  - m_Path: Circle001/Cylinder009/GeoSphere008/GeoSphere012
    m_Weight: 1
  - m_Path: Circle001/Cylinder009/GeoSphere008/GeoSphere012/Bone001
    m_Weight: 1
  - m_Path: Circle001/Cylinder009/GeoSphere008/GeoSphere012/Bone001/Object001
    m_Weight: 1
  - m_Path: Circle001/Cylinder009/GeoSphere008/GeoSphere012/Bone001/Object001/Bone004
    m_Weight: 1
  - m_Path: Circle001/Cylinder009/GeoSphere013
    m_Weight: 1
  - m_Path: Circle001/Cylinder009/GeoSphere013/GeoSphere014
    m_Weight: 1
  - m_Path: Circle001/Cylinder009/GeoSphere013/GeoSphere014/Bone003
    m_Weight: 1
  - m_Path: Circle001/Cylinder009/GeoSphere013/GeoSphere014/Bone003/Object002
    m_Weight: 1
  - m_Path: Circle001/Cylinder009/GeoSphere013/GeoSphere014/Bone003/Object002/Bone005
    m_Weight: 1
