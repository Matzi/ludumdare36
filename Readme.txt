============================================ 
== The Golem 
============================================

Created as an entry for Ludum Dare 36 on the 72 hour game jam. 
http://ludumdare.com/compo/ludum-dare-36/?action=preview&uid=56958

Control the mighty Golem who is created to stop mankind from destroying itself. 
Is it even possible? Who will stop you in the end?

Art by Balázs Szelecki 
Programming by Mátyás Bula

Contact: matzigon AT gmail DOT com

Music is free to use resource.
=============================================
This work is licensed under CC 3.0 http://creativecommons.org/licenses/by/3.0/